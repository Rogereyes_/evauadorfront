import { Component, OnInit, Input } from '@angular/core';
import { Pregunta } from '../pregunta';
import { PREGUNTAS } from '../mock-preguntas';
import { Cuestionario} from '../cuestionario';
import { PreguntasService } from '../servicios/preguntas/preguntas.service';

@Component({
  selector: 'app-preguntas',
  templateUrl: './preguntas.component.html',
  styleUrls: ['./preguntas.component.css']
})
export class PreguntasComponent implements OnInit {
  @Input() cuestionarioSeleccionado: any = {};
  
  preguntas: any;
  opciones: any;
  preguntaSeleccionada?: any;
  constructor(private preguntaservice: PreguntasService) { }

  ngOnInit(): void {
    this.preguntaservice.getpreguntas(this.cuestionarioSeleccionado.id).subscribe(response => {
      this.preguntas = response.results;
    })
  }

  onSelect(pregunta: Pregunta): void{ 
    this.preguntaSeleccionada = null;
    this.preguntaSeleccionada = pregunta; 
    console.log(this.preguntaSeleccionada.question_text);
  }
}
