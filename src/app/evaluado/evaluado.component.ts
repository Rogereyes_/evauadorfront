import { Component, Input, OnInit } from '@angular/core';
import { PreguntasService} from '../servicios/preguntas/preguntas.service';

@Component({
  selector: 'app-evaluado',
  templateUrl: './evaluado.component.html',
  styleUrls: ['./evaluado.component.css']
})
export class EvaluadoComponent implements OnInit {
  evaluado:any;
  cedula:any;
  constructor(private  preguntaservice : PreguntasService) { }

  ngOnInit(): void {
    
  }
  obtenerEvaluado(): void {
    this.preguntaservice.getRegistro(this.cedula).subscribe(response => {
      this.evaluado=response.results;
      this.preguntaservice.setEvaluado(this.evaluado.id);
      console.log(this.evaluado.cedula);
    });
  }

}
