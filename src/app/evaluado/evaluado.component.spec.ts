import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluadoComponent } from './evaluado.component';

describe('EvaluadoComponent', () => {
  let component: EvaluadoComponent;
  let fixture: ComponentFixture<EvaluadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EvaluadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
