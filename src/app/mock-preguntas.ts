import { Pregunta } from './pregunta';

export const PREGUNTAS: Pregunta[] = [
  { cuestionario: 'Primer cuestionario',id: 1, text: 'Pregunta 1.1' },
  { cuestionario: 'Primer cuestionario',id: 2, text: 'Pregunta 1.2' },
  { cuestionario: 'Primer cuestionario',id: 3, text: 'Pregunta 1.3' },
  { cuestionario: 'Segundo cuestionario',id: 4, text: 'Pregunta 2.1' },
  { cuestionario: 'Segundo cuestionario',id: 5, text: 'Pregunta 2.2' },
  { cuestionario: 'Segundo cuestionario',id: 6, text: 'Pregunta 2.3' }
];