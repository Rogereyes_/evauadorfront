import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PreguntasService {

  private headers = new HttpHeaders({
    "Content-Type": "application/json"
  });
  private cuestionario: number = 0;
  private evaluado:any ;
  constructor(private http:HttpClient) { }
  getRegistro(cedula:number){
    return this.http.get(`http://localhost:8000/api/registro-evaluado/?cedula=${cedula}`,{headers:this.headers}).pipe(
      map((response:any)=>{
        console.log(response);
        return response;
      })
    );
  }
  getpreguntas(idcuestionario:number){
    return this.http.get(`http://localhost:8000/api/question-list?cuestionario=${idcuestionario}`,{headers:this.headers}).pipe(
      map((response:any)=>{
        console.log(response);
        return response;
      })
    );
  }
  getCuestionarios(){
    return this.http.get("http://localhost:8000/api/cuestionarios",{headers:this.headers}).pipe(
      map((response:any)=>{
        console.log(response);
        return response;
      })
    );
  }
  getOpciones(idpregunta:number){
    return this.http.get(`http://localhost:8000/api/choice-list?pregunta=${idpregunta}`,{headers:this.headers}).pipe(
      map((response:any)=>{
        console.log(response);
        return response;
      })
    );
  }
  guardarRespuesta(idpregunta:number, correct: boolean){
    let request = {
      cuestionario: this.cuestionario,
      evaluado: this.evaluado,
      question: idpregunta,
      correct: correct
    }
    const body = JSON.stringify(request);
    return this.http.post(`http://localhost:8000/api/respuestas/`, body, {headers:this.headers}).pipe(
      map((response:any)=>{
        console.log(response);
        return response;
      })
    );
  }
  getEvaluado(){
    return this.evaluado
  }
  setEvaluado(evaluado:number){
    this.evaluado = evaluado;
    console.log(evaluado);
  }
  getcuestionario(){
    return this.cuestionario
  }
  setCuestionario(cuestionario:number){
    this.cuestionario = cuestionario;
  }
}
