import { Cuestionario } from './cuestionario';

export const CUESTIONARIOS: Cuestionario[] = [
    { name: 'Primer cuestionario' },
    { name: 'Segundo cuestionario' }
  ];