import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PreguntasComponent } from './preguntas/preguntas.component';
import { OpcionesComponent } from './opciones/opciones.component';
import { CuestionarioComponent } from './cuestionario/cuestionario.component';
import { EvaluadoComponent } from './evaluado/evaluado.component';

@NgModule({
  declarations: [
    AppComponent,
    PreguntasComponent,
    OpcionesComponent,
    CuestionarioComponent,
    EvaluadoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
