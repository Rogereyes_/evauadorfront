import { Component, OnInit, Input } from '@angular/core';
import { Pregunta } from '../pregunta'
import { PreguntasService } from '../servicios/preguntas/preguntas.service'

@Component({
  selector: 'app-opciones',
  templateUrl: './opciones.component.html',
  styleUrls: ['./opciones.component.css']
})
export class OpcionesComponent implements OnInit {

  @Input() preguntaSeleccionada: any;
  opciones: any;
  optionValues:any[] = [];
  constructor(private preguntasservice: PreguntasService) { }

  ngOnInit(): void {
    this.preguntasservice.getOpciones(this.preguntaSeleccionada.id).subscribe(response => {
      this.opciones = response.results;
    });
  }
  validateCheckBoxQuestion(event: Event) {
    let optionValue = {
      idOpcion: (<HTMLInputElement>event.target).id, // Este Id es un string de la forma: 'opcion(idOpcion)'
      valor: (<HTMLInputElement>event.target).value
    }
    let opcionAnterior = this.preguntaSeleccionada.opcionSeleccionada;
    let opcionAnteriorEsLaMismaNueva = opcionAnterior?.idOpcion == optionValue.idOpcion ? true : false;
    if (opcionAnterior) {
      // Se desmarca el checkbox de la opcion anteriormente seleccionada
      let checkBoxOpcionAnterior = document.getElementById(opcionAnterior.idOpcion) as HTMLInputElement;
      checkBoxOpcionAnterior.checked = false;
    }
    this.preguntaSeleccionada.opcionSeleccionada= opcionAnteriorEsLaMismaNueva ? null : optionValue; // Se agrega la nueva opcion seleccionada
  }
  enviarRespuesta(){
    this.preguntasservice.guardarRespuesta(this.preguntaSeleccionada.id,this.preguntaSeleccionada.opcionSeleccionada.valor).subscribe(response =>{
      alert("Se ha guardado su respuesta correctamente.")
    });
  }
}
