export interface Pregunta {
    cuestionario: string;
    id: number;
    text: string;
  }