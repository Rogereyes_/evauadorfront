import { Component, OnInit } from '@angular/core';
import { Cuestionario } from '../cuestionario';
import { CUESTIONARIOS } from '../mock-cuestionario';
import { PreguntasService} from '../servicios/preguntas/preguntas.service';

@Component({
  selector: 'app-cuestionario',
  templateUrl: './cuestionario.component.html',
  styleUrls: ['./cuestionario.component.css']
})
export class CuestionarioComponent implements OnInit {
  cuestionarios: any[] = [];
  cuestionarioSeleccionado: any;  

  constructor(private preguntaservice : PreguntasService) { }

  ngOnInit(): void {
    this.preguntaservice.getCuestionarios().subscribe(response => {
      this.cuestionarios=response.results;
    });
  }
  onSelect(cuestionario: Cuestionario): void{ 
    this.cuestionarioSeleccionado = cuestionario;
    this.preguntaservice.setCuestionario(this.cuestionarioSeleccionado.id);
  }
}
